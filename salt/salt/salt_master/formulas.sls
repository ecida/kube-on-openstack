formulas_gitfs_required_packages:
  pkg.installed:
    - pkgs:
        - git
        - python3-git

kubernetes_formula_git_cloned:
  git.latest:
    - name: https://github.com/saltstack-formulas/kubernetes-formula
    - rev: v1.3.1
    - target: /srv/formulas/kubernetes-formula

formulas_config_file:
  file.managed:
    - name: /etc/salt/master.d/formulas.conf
    - require:
        - pkg: formulas_gitfs_required_packages
    - watch_in:
        - service: salt_master_service
    - contents: |
        fileserver_backend:
          - gitfs
        gitfs_remotes:
          - file:///srv/formulas/kubernetes-formula