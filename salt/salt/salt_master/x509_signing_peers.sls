formulas_config_file:
  file.managed:
    - name: /etc/salt/master.d/peer.conf
    - watch_in:
        - service: salt_master_service
    - contents: |
        peer:
          .*:
            - x509.sign_remote_certificate