{% set kubernetes_ca_path = "/etc/pki/kubernetes-ca" %}

kubernetes_ca_dir:
  file.directory:
    - name: {{ kubernetes_ca_path }}
    - makedirs: True

kubernetes_ca_issued_dir:
  file.directory:
    - name: {{ kubernetes_ca_path }}/issued_certificates
    - makedirs: True

{% set ca_key_path = kubernetes_ca_path+"/ca.key" %}

kubernetes_ca_private_key:
  x509.private_key_managed:
    - name: {{ ca_key_path }}
    - bits: 4096
    - new: True
    - backup: True
  {% if salt['file.file_exists'](ca_key_path)  %}
  {# If the key already exists, check the validity of the cert before recreating #}
    - prereq:
        - x509: kubernetes_ca_cert
  {% endif %}

kubernetes_ca_cert:
  x509.certificate_managed:
    - name: {{ kubernetes_ca_path }}/ca.crt
    - signing_private_key: {{ ca_key_path }}
    - CN: ca.{{ pillar.get('terraform_vars:deploy_domain', 'kube-on-os') }}
    - C: NL
    - ST: Groningen
    - L: Groningen
    - basicConstraints: "critical CA:true"
    - keyUsage: "critical cRLSign, keyCertSign"
    - subjectKeyIdentifier: hash
    - authorityKeyIdentifier: keyid,issuer:always
    - days_valid: {{ 4*365 }}
    - days_remaining: 90
    - backup: True

kubernetes_ca_minion_config:
  file.managed:
    - name: /etc/salt/minion.d/x509.conf
    - onchanges_in:
        - service: salt_minion_service
    - contents: |
        mine_functions:
          x509.get_pem_entries: [{{ kubernetes_ca_path }}/ca.crt]

        x509_signing_policies:
          kubernetes:
            - signing_private_key: {{ kubernetes_ca_path }}/ca.key
            - signing_cert: {{ kubernetes_ca_path }}/ca.crt
            - C: NL
            - ST: Groningen
            - L: Groningen
            - basicConstraints: "critical CA:false"
            - keyUsage: "critical keyEncipherment"
            - subjectKeyIdentifier: hash
            - authorityKeyIdentifier: keyid,issuer:always
            - days_valid: 90
            - copypath: {{ kubernetes_ca_path }}/issued_certificates/