{#
kubernetes_repo:
  pkgrepo.managed:
    - humanname: Kubernetes Repository
    - name: deb https://apt.kubernetes.io/ kubernetes-xenial main
    - dist: kubernetes-xenial
    - key_url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
    - file: /etc/apt/sources.list.d/kubernetes.list
    - gpgcheck: 1
#}

kubernetes_packages_installed:
  pkg.installed:
    - pkgs:
        - docker
        - docker.io
{#
        - kubectl
        - kubeadm
        - kubelet
        - kubernetes-cni
    - require:
        - pkgrepo: kubernetes_repo
#}