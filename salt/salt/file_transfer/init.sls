{#
Simple state for securely transfering files between the master and minions.

Requires that a 'file_transfer' saltenv be defined in the master config using
the file_roots option.
#}

{% for entry_name, options in salt['pillar.get']('file_transfer', {}).items() %}

{{entry_name}}-file-transfer:
  file.managed:
    - name: {{options.get('destination')}}
    - source: {{options.get('source')}}
    - user: {{options.get('user', 'root')}}
    - group: {{options.get('group', 'root')}}
    - mode: {{options.get('mode', '600')}}
    - dir_mode: {{options.get('dir_mode', '700')}}
    - makedirs: True
    - saltenv: file_transfer

{% endfor %}

{% for entry_name, options in salt['pillar.get']('file_contents', {}).items() %}

{{entry_name}}-file-contents:
  file.managed:
    - name: {{options.get('destination')}}
    - user: {{options.get('user', 'root')}}
    - group: {{options.get('group', 'root')}}
    - mode: {{options.get('mode', '600')}}
    - dir_mode: {{options.get('dir_mode', '700')}}
    - makedirs: True
    - saltenv: file_transfer
    - contents_pillar: file_contents:{{entry_name}}:contents
{% endfor %}
