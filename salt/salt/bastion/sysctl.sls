bastion_sysctl_ip_forward:
  sysctl.present:
    - name: net.ipv4.ip_forward
    - value: 1

bastion_sysctl_all_interfaces_forwarding:
  sysctl.present:
    - name: net.ipv4.conf.all.forwarding
    - value: 1