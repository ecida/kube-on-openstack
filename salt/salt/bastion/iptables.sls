bastion_iptables_installed:
  pkg.installed:
    - pkgs:
      - iptables
      - iptables-persistent

bastion_iptables_rules:
  file.managed:
    - name: /etc/iptables/rules.v4
    - contents: |
        *nat
        :PREROUTING ACCEPT [2:76]
        :INPUT ACCEPT [2:76]
        :OUTPUT ACCEPT [0:0]
        :POSTROUTING ACCEPT [0:0]
        -A POSTROUTING -o ens3 -j MASQUERADE
        COMMIT

        *filter
        :INPUT ACCEPT [31:2088]
        :FORWARD ACCEPT [0:0]
        :OUTPUT ACCEPT [26:2396]
        -A FORWARD -i ens4 -o ens3 -m conntrack --ctstate NEW -j ACCEPT
        -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
        COMMIT

bastion_iptables_restore:
  cmd.wait:
    - name: iptables-restore < /etc/iptables/rules.v4
    - watch:
        - file: bastion_iptables_rules
    - require:
        - pkg: bastion_iptables_installed
