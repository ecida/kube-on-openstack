motd_issue:
  file.managed:
    - name: /etc/issue
    - source: salt://core/files/_etc_issue
    - template: jinja
    - user: root
    - group: root
    - mode: 0644

motd_issue_net:
  file.managed:
    - name: /etc/issue.net
    - source: salt://core/files/_etc_issue
    - template: jinja
    - user: root
    - group: root
    - mode: 0644

motd_issue_minimal:
  file.managed:
    - name: /etc/issue.minimal
    - source: salt://core/files/_etc_issue
    - template: jinja
    - user: root
    - group: root
    - mode: 0644

motd_etc_motd:
  file.managed:
    - name: /etc/motd
    - source: salt://core/files/_etc_motd
    - template: jinja
    - user: root
    - group: root
    - mode: 0644

motd_disable_ubuntu_motd_generator:
  # Disables execution on the motd scripts to non-destructively disable them.
  file.directory:
    - name: /etc/update-motd.d
    - file_mode: 644
    - recurse:
        - mode
