#!py


def run():
    """
    Puts every minion in the hostsfile of every other minion such that we can
    reference them by name.

    This is a simple and easy way to ensure local addresses are always
    resolvable, but it does rely on the salt mine being active.

    Running this state forces an update of the mine.
    """
    config = {}

    # Update the mine so we have the latest data:
    __salt__['mine.update']()

    for minion_name, addrs in __salt__['mine.get']("*", "network.ip_addrs").items():
        if minion_name == __grains__['id']:
            addrs = ["127.0.0.1"]

        config[f"hostsfile_{minion_name}"] = {
            "host.present": [
                {"names": [
                    minion_name.replace("_", "").split('.')[0],
                    minion_name.replace("_", ""),
                ]},
                {"ip": addrs},
                {"clean": True}
            ]
        }

    return config
