sysctl_max_open_files:
  sysctl.present:
    - name: fs.file-max
    - value: 16384

sysctl_disable_ipv6:
  sysctl.present:
    - name: net.ipv6.conf.all.disable_ipv6
    - value: 1

sysctl_disable_ipv6_default:
  sysctl.present:
    - name: net.ipv6.conf.default.disable_ipv6
    - value: 1

max_open_file_security_limits:
  file.line:
    - name: /etc/security/limits.conf
    - mode: ensure
    - after: end
    - content: "* - nofiles 16384"
