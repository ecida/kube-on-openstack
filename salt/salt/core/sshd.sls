ssh_config:
  file.managed:
    - name: /etc/ssh/sshd_config
    - source: salt://{{ slspath }}/files/_etc_ssh_sshd_config
    - template: jinja
    - user: root
    - group: root
    - mode: 600

ssh_service:
  service.running:
    - name: ssh
    - enable: True
    - reload: True
    - onchanges:
      - file: ssh_config
      - file: motd_issue
      - file: motd_issue_net
      - file: motd_issue_minimal
      - file: motd_etc_motd