sudoers_file:
  file.managed:
    - name: /etc/sudoers
    - source: salt://core/files/_etc_sudoers
    - user: root
    - group: root
    - mode: 440
