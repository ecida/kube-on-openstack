set_resolvers:
  file.managed:
    - name: /etc/resolv.conf
    - contents:
      - "nameserver 1.1.1.1"
      - "nameserver 1.0.0.1"