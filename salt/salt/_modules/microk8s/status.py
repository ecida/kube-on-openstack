"""
Module for managing the status of microk8s
"""

import yaml
from yaml.scanner import ScannerError

from .crosscall import __salt__
from .constants import MICROK8S_PATH


def is_installed():
    """
    Returns whether microk8s is installed or not
    :return: bool
    """
    if __salt__["file.file_exists"](MICROK8S_PATH):
        return True
    else:
        return False


def status():
    """
    Gets the current status of the microk8s running on the system
    :return: str
    """
    if not is_installed():
        return {"microk8s": {"running": False}}

    status_command = f"{MICROK8S_PATH} status --format yaml"

    status_command_output = __salt__["cmd.run"](status_command)

    if "not running" in status_command_output:
        return {"microk8s": {"running": False}}
    try:
        return yaml.load(status_command_output)
    except Exception as e:
        print("Parsing current status of Microk8s failed on reading status command output" +
              f"Command that was run: {status_command}.\n" +
              f"Command output:\n{status_command_output}\n" +
              f"Exception: {e}"
        )


def is_running():
    """
    Returns whether microk8s is running.
    :return:
    """
    return status()["microk8s"]["running"]


def start():
    """
    Starts microk8s.
    :return: cluster status after starting.
    """
    run_command = f"{MICROK8S_PATH} start"
    run_command_output = __salt__["cmd.run"](run_command)

    wait_ready_command = f"{MICROK8S_PATH} status --wait-ready --format yaml"
    wait_ready_command_output = __salt__["cmd.run"](wait_ready_command)

    return yaml.load(wait_ready_command_output)
