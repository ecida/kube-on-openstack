"""
Module for managing microk8s addons.
"""

import time

from .status import status

from .crosscall import __salt__
from .constants import MICROK8S_PATH


def addons():
    """
    Gets all the available addons and their status.
    :return:
    """
    return status()["addons"]


def addons_enabled():
    """
    Gets all currently enabled addons.
    """
    if "addons" not in status().keys():
        return []
    enabled_addons = [a for a in addons() if a["status"] == "enabled"]
    return enabled_addons


def addons_disabled():
    """
    Gets all currently disabled addons.
    """
    if "addons" not in status().keys():
        return []
    disabled_addons = [a for a in addons() if a["status"] == "disabled"]
    return disabled_addons


def addon_is_enabled(addon_name):
    """
    Returns true if an addon is enabled, otherwise false.
    :param addon_name: the name of the addon to check
    :return: whether the addon is currently enabled
    """
    for addon in addons_enabled():
        if addon["name"] == addon_name:
            return True
    return False


def enable_addon(addon_name, env=None):
    """
    Enables a microk8s addon.
    Does nothing if the addon is already enabled.
    :param env: environment variables to inject when running the enable command
    :param addon_name: the name of the addon to enable
    :return: command output
    """
    if env is None:
        env = {"LANG": "C.UTF-8", "LC_ALL": "C.UTF-8"}

    if addon_is_enabled(addon_name):
        return f"the addon '{addon_name}' already enabled"

    enable_command = f"{MICROK8S_PATH} enable {addon_name}"
    enable_command_output = __salt__["cmd.run"](enable_command, env=env)

    time.sleep(1)  # The addon may not instantly report as being ready.

    # Check if operation was successful:
    if addon_is_enabled(addon_name):
        return f"the addon '{addon_name}' has been succesfully enabled\n{enable_command_output}"
    return (
        f"an error occurred when enabling addon '{addon_name}'\n"
        + f"output: {enable_command_output}\ncurrent status:{status()}"
    )


def disable_addon(addon_name, env=None):
    """
    Enables a microk8s addon.
    Does nothing if the addon is already enabled.
    :param env: environment variables to inject when running the disable command
    :param addon_name: the name of the addon to enable
    :return: command output
    """
    if env is None:
        env = {"LANG": "C.UTF-8", "LC_ALL": "C.UTF-8"}

    if not addon_is_enabled(addon_name):
        return f"the addon '{addon_name}' is already disabled"

    disable_command = f"{MICROK8S_PATH} disable {addon_name}"
    disable_command_output = __salt__["cmd.run"](disable_command, env=env)

    # Check if operation was successful:
    if not addon_is_enabled(addon_name):
        return f"the addon '{addon_name}' has been succesfully disabled:\n{disable_command_output}"
    return (
        f"an error occurred when disabling addon '{addon_name}'\n"
        + f"output: {disable_command_output}\ncurrent status:{status()}"
    )
