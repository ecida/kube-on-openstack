microk8s_enable_ingress:
  microk8s.addon_enabled:
    - name: ingress
    - require:
        - microk8s: microk8s_started

microk8s_enable_dashboard:
  microk8s.addon_enabled:
    - name: dashboard
    - require:
        - microk8s: microk8s_started
