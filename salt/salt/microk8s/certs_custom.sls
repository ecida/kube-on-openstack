
{% if salt['pillar.get']('microk8s:allowed_csr_ip')  %}
microk8s_csr_template:
  file.line:
    - name: /var/snap/microk8s/current/certs/csr.conf.template
    - user: root
    - group: microk8s
    - mode: 0660
    - mode: ensure
    - after: "#MOREIPS"
    - content: "IP.3 = {{ salt['pillar.get']('microk8s:allowed_csr_ip') }}"
{% endif  %}

microk8s_certs_refreshed:
  cmd.wait:
    - name: "/snap/bin/microk8s refresh-certs --cert ca.crt"
    - env:
        - LC_ALL: C.UTF-8
        - LANG: C.UTF-8
    - watch:
        - file: microk8s_csr_template
