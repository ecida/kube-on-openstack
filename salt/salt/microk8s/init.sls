include:
  - {{ slspath }}/installed
  {% if salt['pillar.get']("microk8s:master")
     and grains['id'] != salt['pillar.get']("microk8s:master")
  %}
  - {{ slspath }}/joined
  {% endif %}