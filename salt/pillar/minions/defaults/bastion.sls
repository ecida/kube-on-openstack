network:
  external_interface: ens3
  external_ip: 10.0.0.10
  external_gateway: 10.0.0.1

  internal_interface: ens4
  internal_ip: 10.1.0.1