# Kubespray
I've found a project on github called Kubespray that seems active and well-maintained. It's even mentioned in the official kubernetes setup documentation, though you have to dig a little: https://kubernetes.io/docs/setup/production-environment/tools/kubespray/
It seems to do a lot of what I'm wanting to do. I it can prevent me from reinventing the wheel, then that could be a very nice thing.

It uses Terraform and Ansible to deploy the kubernetes cluster. Terraform I already know and like. Ansible would be new, but what better time to learn? :)

This could save me a lot of work, but also provide a better platform that I could, especially on the long term. It might mean changing my plan entirely, but that's not a problem. Kill your darlings :).

- [ ] Deploy kube with Kubespray to see how it works

Link to github repo:
https://github.com/kubernetes-sigs/kubespray

Today I'll try to follow this guide:
https://rsmitty.github.io/Terraform-Ansible-Kubernetes/

Update 10:40: The guide is very basic and involves manual work. I'll still have to automate around, but that still could be very much worth worth the effort.

Update 15:14: Since kubespray uses Ansible - and I prefer simplicity over complexity - I'm kicking out salt for the duration of this experiment. If successful, I'll switch to Ansible.

## The hard way
Should the above not turn out the way I hope, I can always go the hard way:
https://kubernetes.io/docs/setup/production-environment/
https://github.com/kelseyhightower/kubernetes-the-hard-way

- [ ] Evaluate kubespray deploy, and on fail go the hard way