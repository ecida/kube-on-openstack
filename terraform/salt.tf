resource "openstack_networking_port_v2" "salt_private" {
  name           = "salt_private_port"
  network_id     = openstack_networking_network_v2.private.id
  admin_state_up = "true"

  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.private.id
    ip_address = "10.1.0.5"
  }
}

resource "openstack_blockstorage_volume_v3" "salt-persistence" {
  size                 = 50
  description          = "Stores the state of the salt master. Mounted to /srv/"
  enable_online_resize = true
}

resource "openstack_compute_instance_v2" "salt" {
  name            = "salt.${var.deploy_domain_name}"
  security_groups = ["default"]
  flavor_name     = "m1.small"

  tags = []

  user_data = templatefile("${path.module}/cloud_init/salt.tftpl", {
    private_ip         = openstack_networking_port_v2.salt_private.fixed_ip[0].ip_address,
    gateway_ip         = openstack_networking_port_v2.bastion_private.fixed_ip[0].ip_address,
    set_root_password  = var.set_root_password,
    root_password      = var.root_password,
    hostname           = "salt",
    fqdn               = "salt.${var.deploy_domain_name}",
    install_script     = filebase64("${path.module}/scripts/install_salt_master.sh"),
    wait_for_it_script = filebase64("${path.module}/scripts/wait-for-it.sh")
  })
  key_pair = openstack_compute_keypair_v2.deploy_key.id

  depends_on = [openstack_compute_instance_v2.bastion]

  block_device {
    boot_index            = 0 # First in the boot-order
    volume_size           = 10
    source_type           = "image"
    destination_type      = "volume"
    uuid                  = var.base_image
    delete_on_termination = true
  }

  block_device {
    source_type      = "volume"
    uuid             = openstack_blockstorage_volume_v3.salt-persistence.id
    boot_index       = -1 # Never boot from this
    guest_format     = "ext4"
    destination_type = "volume"
  }

  network {
    port = openstack_networking_port_v2.salt_private.id
  }

  connection {
    type = "ssh"
    user = var.deploy_user
    host = openstack_networking_port_v2.salt_private.fixed_ip[0].ip_address

    private_key = openstack_compute_keypair_v2.deploy_key.private_key

    bastion_host        = openstack_networking_floatingip_associate_v2.bastion_float.floating_ip
    bastion_user        = var.deploy_user
    bastion_private_key = openstack_compute_keypair_v2.deploy_key.private_key
  }

  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
      "sudo bash /tmp/install_script.sh"
    ]
  }
}

resource "null_resource" "sync_salt_states" {
  # This resource manages changes to the salt file structure.
  # This is separate from the salt-master instance resource to prevent it being recreated when the salt dir changes.

  depends_on = [
    openstack_blockstorage_volume_v3.salt-persistence,
    openstack_compute_instance_v2.salt
  ]

  triggers = {
    # Easy way to watch all files in a dir for changes
    # Creates a long string of concatenated file hashes, and then hashes that.
    # If one file changes, so does the end result.
    dir_hash = sha1(join("", [for filename in fileset("../salt", "*") : filesha1(filename)]))
  }

  connection {
    type = "ssh"
    user = var.deploy_user
    host = openstack_networking_port_v2.salt_private.fixed_ip[0].ip_address

    private_key = openstack_compute_keypair_v2.deploy_key.private_key

    bastion_host        = openstack_networking_floatingip_associate_v2.bastion_float.floating_ip
    bastion_user        = var.deploy_user
    bastion_private_key = openstack_compute_keypair_v2.deploy_key.private_key
  }

  provisioner "file" {
    source      = "${path.module}/../salt"
    destination = "/tmp/newsrv"
  }

  provisioner "file" {
    content = templatefile("${path.module}/templates/_srv_pillar_from_terraform.sls", {
      users             = var.users,
      deploy_user       = var.deploy_user,
      deploy_public_key = openstack_compute_keypair_v2.deploy_key.public_key,
      deploy_domain     = var.deploy_domain_name
    })
    destination = "/tmp/newsrv/pillar/_from_terraform.sls"
  }

  provisioner "remote-exec" {
    inline = [
      "set -x",
      "sudo rm -rf /srv/salt /srv/pillar /srv/reactor /srv/formulas",
      "sudo cp -rv /tmp/newsrv/* /srv",
      "sudo rm -rf /tmp/newsrv",
      "sudo chown -R root: /srv/salt /srv/pillar /srv/reactor /srv/file_transfer",
      # Set up formula
      "sudo mkdir -p /srv/formulas",
      "cd /srv/formulas",
      "sudo git clone -v -b ${var.kubernetes_formula_version} ${var.kubernetes_formula_url}",
      # Apply any changes to the master's configuration:
      "sudo salt salt\\* state.highstate",
      "sudo salt \\* saltutil.sync_all",
    ]
  }
}
