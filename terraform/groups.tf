resource "openstack_compute_servergroup_v2" "kubernetes_group" {
  # Using this group we can tell openstack not to schedule all kube vms on the same hypervisor
  # I've kept the policy "soft" to prevent failures when the cluster gets bigger than the hypervisor count
  name     = "Kubernetes Group - ${var.deploy_domain_name}"
  policies = ["soft-anti-affinity"]
}