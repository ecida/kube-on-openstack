resource "openstack_compute_instance_v2" "k8s_worker" {
  name = "k8s_worker-${count.index}.${var.deploy_domain_name}"

  count = var.num_kube_nodes - 1

  security_groups = ["default"]
  key_pair        = openstack_compute_keypair_v2.deploy_key.id
  flavor_name     = var.kube_node_flavor

  user_data = templatefile("${path.module}/cloud_init/salt-minion.tftpl", {
    gateway_ip        = openstack_networking_floatingip_associate_v2.bastion_float.floating_ip,
    set_root_password = var.set_root_password,
    root_password     = var.root_password,
    hostname          = "k8s_worker-${count.index}",
    fqdn              = "k8s_worker-${count.index}.${var.deploy_domain_name}",
    salt_ip           = openstack_networking_port_v2.salt_private.fixed_ip[0].ip_address
    install_script    = filebase64("${path.module}/scripts/install_salt_minion.sh")
  })

  depends_on = [openstack_compute_instance_v2.k8s_master]

  network {
    uuid        = openstack_networking_network_v2.private.id
    fixed_ip_v4 = "10.1.0.${100 + count.index}"
  }

  block_device {
    boot_index            = 0
    delete_on_termination = true
    source_type           = "image"
    destination_type      = "volume"
    uuid                  = var.base_image
    volume_size           = 50
  }

  scheduler_hints {
    group = openstack_compute_servergroup_v2.kubernetes_group.id
  }

  connection {
    type = "ssh"
    user = var.deploy_user
    host = self.network[0].fixed_ip_v4

    private_key = openstack_compute_keypair_v2.deploy_key.private_key

    bastion_host        = openstack_networking_floatingip_associate_v2.bastion_float.floating_ip
    bastion_user        = var.deploy_user
    bastion_private_key = openstack_compute_keypair_v2.deploy_key.private_key
  }


  provisioner "remote-exec" {
    # Delete old keys that may have remained from a previous deploy with the same name.
    # Attention! Executed on the salt-master

    connection {
      host = openstack_networking_port_v2.salt_private.fixed_ip[0].ip_address
    }

    inline = [
      "sudo salt-key --yes --delete ${self.name}" # In case we're replacing the instance
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
      "sudo bash /tmp/install_script.sh"
    ]
  }

  provisioner "remote-exec" {
    # Accept this minion's key and run states
    # Attention! Executed on the salt-master

    connection {
      host = openstack_networking_port_v2.salt_private.fixed_ip[0].ip_address
    }

    inline = [
      "sudo salt-key --yes --accept ${self.name}"
    ]
  }
}
