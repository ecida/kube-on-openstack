resource "openstack_compute_keypair_v2" "deploy_key" {
  name = "terraform_deploy_keypair"

  lifecycle {
    prevent_destroy = true
  }
}

resource "openstack_networking_secgroup_v2" "ssh_all" {
  name        = "ssh_all"
  description = "Allows ssh port 22 connections from all ips"
}

resource "openstack_networking_secgroup_rule_v2" "ssh_all_ingress_rule" {
  security_group_id = openstack_networking_secgroup_v2.ssh_all.id
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_max    = 22
  port_range_min    = 22
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_v2" "all_traffic" {
  name        = "all_traffic"
  description = "Allows all traffic into this machine. Usefule for, for example, a node running microk8s ingress."
}

resource "openstack_networking_secgroup_rule_v2" "all_tcp_ingress_rule" {
  security_group_id = openstack_networking_secgroup_v2.ssh_all.id
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_max    = 65535
  port_range_min    = 1
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "all_udp_ingress_rule" {
  security_group_id = openstack_networking_secgroup_v2.ssh_all.id
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_max    = 65535
  port_range_min    = 1
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "basic_http" {
  security_group_id = openstack_networking_secgroup_v2.all_traffic.id
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "basic_https" {
  security_group_id = openstack_networking_secgroup_v2.all_traffic.id
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
}
